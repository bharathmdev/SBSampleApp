//
//  CoffeeModel.swift
//  SBSampleApp
//
//  Created by bharath medimalli on 12/1/22.
//

import Foundation


struct CoffeeModel: Identifiable {
    var id: Int
    var name: String
    var Description: String
    var Ingredients: String
    var image: String
    
}
