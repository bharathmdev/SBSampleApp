//
//  ContentView.swift
//  SBSampleApp
//
//  Created by bharath medimalli on 12/1/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        CoffeeListView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        CoffeeListView()
    }
}
