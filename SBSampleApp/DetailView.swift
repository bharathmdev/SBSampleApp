//
//  DetailView.swift
//  SBSampleApp
//
//  Created by bharath medimalli on 12/1/22.
//

import Foundation
import SwiftUI


struct DetailsView: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    @ObservedObject var viewModel: DetailsViewModel
    
    init(model: CoffeeModel) {
        viewModel = DetailsViewModel(model: model)
    }
    
    var body: some View {
        NavigationView {
            ZStack {
                ScrollView(.vertical, showsIndicators: false) {
                    VStack(spacing: 16) {
                        Image(viewModel.model.image).resizable()
                            .frame(height: 400).frame(maxWidth: .infinity).cornerRadius(10)
                        Text(viewModel.model.Description).font(Font.system(size: 13))
                            .foregroundColor(Color.black)
                        
                        Text(viewModel.model.Ingredients).font(Font.system(size: 13))
                            .foregroundColor(Color.black)
                        
                    }
                    .background(Color.white)
                    .padding(.top, 120)
                    .padding(.horizontal, 16)
                    
                    Spacer()
                    Spacer().frame(height: 150)
                }
                .edgesIgnoringSafeArea(.all)
                .navigationBarHidden(true)
            }
        }
        .navigationViewStyle(StackNavigationViewStyle())
        .navigationTitle(Text(viewModel.model.name))
        .navigationBarTitleDisplayMode(.inline)
    }
}


