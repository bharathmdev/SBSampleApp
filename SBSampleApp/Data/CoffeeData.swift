//
//  CoffeeData.swift
//  SBSampleApp
//
//  Created by bharath medimalli on 12/1/22.
//


import Foundation

struct CoffeeData {
    static let Coffees = [
        CoffeeModel(id: 1001, name: "Holiday Blend Café Au Lait with Cherry Almond Sweet Cream", Description: "Starbucks® Holiday Blend, cherry cream and just a hint of almond? Sounds like the perfect way to top off the holidays. From that first sip, this delightful holiday recipe will become the sensation of your season", Ingredients: "2 cup heavy whipping cream , 3 Tbps maraschino cherry juice , 1/2 tsp almond extract , 8 oz brewed Starbucks® Holiday Blend ,4 oz whole milk", image: COFFEE_1),
        CoffeeModel(id: 1002, name: "Sweet & Spiced Gingerbread Coffee", Description: "In this festive recipe, brown sugar, molasses and cinnamon come together to highlight the delicious spiced notes of our Starbucks® Gingerbread Flavored Coffee", Ingredients: "1/2 tsp molasses , 2 tsp brown sugar, plus a sprinkle for garnish , 1/8 tsp cinnamon, plus a sprinkle for garnish , 8 fl oz Starbucks® Gingerbread Flavored Coffee K-Cup® pod, brewed 4 fl oz whole milk", image: COFFEE_2),
        CoffeeModel(id: 1003, name: "Salted Caramel Coffee Martini", Description: "Your perfect fall nightcap. Create your own delicious salty caramel martini, featuring Starbucks® Salted Caramel Mocha Flavored Coffee", Ingredients: "11 cup ice , 4 oz Starbucks® Salted Caramel Mocha Flavored Coffee,2 oz coffee liqueur ,2 oz vodka ,2 Tbsp caramel sauce ,2 oz half-and-half ,Pinch sea salt ,Extra caramel sauce and sea salt flakes to rim glass", image: COFFEE_3),
        CoffeeModel(id: 1004, name: "Pumpkin Spice Cold Foam", Description: "Pumpkin spice on ice. With this Pumpkin Spice Cold Foam recipe, your fall days just got tastier", Ingredients: "4 oz Starbucks® Cold Brew Pumpkin Spice Flavored Concentrate , 4oz water ,4 oz 2% milk ,2 Tbsp pumpkin spice syrup", image: COFFEE_4),
        CoffeeModel(id: 1005, name: "Maple Vanilla and Brown Sugar Coffee", Description: "With maple, vanilla and brown sugar, this recipe puts everything sweet into one delicious cup of coffee. Top it with whipped cream and cinnamon for an extra layer of yum", Ingredients: "8 oz Starbucks® Maple Pecan Flavored Coffee , 1 1/2 tbsp half-and-half , 2 tsp brown sugar , 1/4 tsp vanilla extract , Whipped cream (optional), Cinnamon (optional)", image: COFFEE_5)
    ]
}
