//
//  CoffeeListView.swift
//  SBSampleApp
//
//  Created by bharath medimalli on 12/1/22.
//

import Foundation
import SwiftUI


struct CoffeeListView: View {
    
    @StateObject var viewModel = CoffeeListViewModel()
    
    var body: some View {
        NavigationView {
            ZStack {
                ScrollView(.vertical, showsIndicators: false) {
                    VStack(spacing:16) {
                        HStack {
                            VStack() {
                                Text("Hello, Welcome").font(Font.system(size: 24))
                                    .foregroundColor(Color.black).padding(.top, 16)
                            }
                            Spacer()
                        }
                        
                        Text("List of Products").font(Font.system(size: 20))
                            .foregroundColor(Color.black)
                            .padding(.top, 24).padding(.bottom, 8)
                        ForEach(viewModel.CoffeeList) { model in
                            NavigationLink(destination: DetailsView(model: model), label: {
                                CoffeeListViewModelView(image: model.image, name: model.name)
                                    .padding(.bottom, 4)
                                
                            })
                        }
                    }
                    .padding(.horizontal, 16)
                    Spacer()
                    Spacer().frame(height: 150)
                }
                .padding(.horizontal, 16).padding(.top, 1)
                .navigationBarHidden(true)
            }
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct CoffeeListViewModelView: View {
    
    var image: String, name: String
    
    var body: some View {
        HStack(spacing: 12) {
            Image(image)
                .resizable().scaledToFill()
                .frame(width: 100, height: 100).cornerRadius(16)
            VStack(alignment: .leading, spacing: 12) {
                Text(name).font(Font.system(size: 15)).foregroundColor(Color.black)
                    .padding(.horizontal, 16)
                
            }
            Spacer()
        }
        .padding(16)
        .padding(.horizontal, 16)
        .background(Color.gray)
        .cornerRadius(16)
    }
    
}

