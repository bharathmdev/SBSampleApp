//
//  CoffeeListViewModel.swift
//  SBSampleApp
//
//  Created by bharath medimalli on 12/1/22.
//

import Foundation


import Foundation

class CoffeeListViewModel: ObservableObject {
    
    @Published var CoffeeList = [CoffeeModel]()
    
    init() { getCoffeeList() }
    
    func getCoffeeList() { CoffeeList = CoffeeData.Coffees }
}
